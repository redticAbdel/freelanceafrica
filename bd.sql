-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2019 at 02:21 PM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `freeafrica`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$2y$10$NkJz2JaIMXAy1k.ORF9ecutKEYAd7kQoFhdnjEpUAEyJBW83E0NTy', 'admin', 'jonse01@live.com', '', '2018-12-07 10:50:31', '1550153669:$P$BHG.BQXPVlaIvnXplNPE28hFXstnmJ/', 0, 'admin'),
(4, 'Entreprise2', '$P$BfXurofRZn/JLHZ5S2toTN2twACLKp.', 'entreprise2', 'entre@prise.test', '', '2018-12-11 10:44:33', '1548344754:$P$BgoLzN9BamXnVYLpyE9fgDn9UynKYq0', 0, 'Entreprise2'),
(5, 'FreelancerX', '$P$Bz6HSBt0ULkq7CskYKUv7ARLXs08Fo1', 'freelancerx', 'free@lancer.test', '', '2018-12-17 10:45:48', '1548429858:$P$BJTZUl5DbJnDyToXMbSmRp8y0lpX2G1', 0, 'FreelancerX'),
(6, 'EntrepriseX', '$P$ByXdwDFRwPHVd3f35Hf5GQwLhSqStV0', 'entreprisex', 'xxx@yyy.test', 'https://blank.org/', '2018-12-17 11:43:35', '1548344375:$P$BHoO2k7Nx4FJIpa1cKEFUDbSEia3As/', 0, 'EntrepriseX'),
(7, 'freelancerY', '$P$BkroCK4Ewq5oV46flW4wIb.bkRnbLn/', 'freelancery', 'free@lancery.com', '', '2019-01-24 08:43:01', '1548329376:$P$BpTCa7b8LG5EXWP0bor.saPN//mftn1', 0, 'freelancerY'),
(8, 'firstlast', '$P$BcrqgJrJUcIUxLZRc7.oKktvlxcsba0', 'firstlast', 'first@last.test', 'https://plug.dj/edmspot', '2019-01-25 12:02:49', '1548417783:$P$BwFfvQbLr1aEhRmPJj12nv.KGdOKYv.', 0, 'firstlast'),
(9, 'CompanyName', '$P$B3A0oZPWeQmAuG09hrPcq3zsdX894y/', 'companyname', 'company@company.test', '', '2019-01-25 12:31:58', '1548419531:$P$BkI4/2iFZAuUi4VDK6s8GnIFjVl/SY1', 0, 'CompanyName');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
